//
//  MNGAdsAdapter.h
//  MNG-Ads-SDK
//
//  Created by Ben Salah Med Amine on 12/9/14.
//  Copyright (c) 2014 MNG. All rights reserved.
//

/**
 MNGAdsAdapter is an abstract class that allow communication between the SDK and any Ads server
 */

#import <Foundation/Foundation.h>
#import "MNGPreference.h"
#import "Protocols.h"

typedef NS_ENUM(NSInteger, MNGAdsType) {
    MNGAdsTypeBanner,
    MNGAdsTypeInterstitial,
    MNGAdsTypeNative
};

typedef CGRect MNGAdSize;

extern MNGAdSize const kMNGAdSizeBanner;
extern MNGAdSize const kMNGAdSizeLargeBanner;
extern MNGAdSize const kMNGAdSizeFullBanner;
extern MNGAdSize const kMNGAdSizeLeaderboard;
extern MNGAdSize const kMNGAdSizeMediumRectangle;

@interface MNGAdsAdapter : NSObject

/**
 *The parameters of initialisation
 */

@property NSDictionary *parameters;

/**
  viewController that th ad will be showen
 @warning required in interstitial
 */

@property (weak) UIViewController *viewController;

@property (atomic) BOOL completed;

/**
 *timeout of one ads server
 */

@property NSTimeInterval timeout;


/**
 *Delegates
 */

@property (weak) id<MNGAdsAdapterBannerDelegate> bannerDelegate;

@property (weak) id<MNGAdsAdapterInterstitialDelegate> interstitialDelegate;

@property (weak) id<MNGAdsAdapterNativeDelegate> nativeDelegate;

/** Init the Ads server
 Any Ads server need some parameters to be inited
 
 @param parameters the parameters of initialisation
 
 */

-(id)initWithParameters:(NSDictionary*)parameters;

/** Create a banner view
 request a banner view from the SDK that will be returned in the delegate methods
 
 @param preferences user's preferences
 
 @return success
 */

-(BOOL)createBannerInFrame:(CGRect)frame withPreferences:(MNGPreference*)preferences;
-(BOOL)createBannerInFrame:(CGRect)frame ;

/** Create a interstitial view
 request a interstitial view from the SDK that will be returned in the delegate methods
 
 @param preferences user's preferences

 
 @return success
 */

-(BOOL)createInterstitialWithPreferences:(MNGPreference*)preferences;
-(BOOL)createInterstitial;



/** Create a native Ads view
 request a native object from the SDK that will be returned in the delegate methods
 
 @param preferences user's preferences
 
 @return success
 */

-(BOOL)createNativeWithPreferences:(MNGPreference*)preferences;
-(BOOL)createNative;

-(void)releaseMemory;

@end
