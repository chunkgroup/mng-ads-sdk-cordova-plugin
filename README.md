#![MNG-Ads-1.png](https://bitbucket.org/repo/aen579/images/3739691856-MNG-Ads-1.png) for Cordova Ios and Android

You can see explanation for native [MngAds Ios] or [MngAds Android] SDKs. Our single [mng-ads-sdk-cordova-plugin] can be used for Ios and Android Apps


## Install with the Cordova CLI

Can you please Prerequisites and Installing procedure of [Cordova CLI].

### Create the App
```
#!unix
$ sudo npm install -g cordova
$ cordova create mngAdsCordovaDemo com.cordova.mngads
$ cd mngAdsCordovaDemo
```

### Add Platforms

```
#!unix

$ cordova platform add android
$ cordova platform add ios
$ cordova platforms ls
```

### Add Plugins

You must download or clone [mng-ads-sdk-cordova-plugin] repository. Then add it to your project.

```
#!unix

$ cordova plugin add ../mng-ads-sdk-cordova-plugin
```

On previous version < cordova 5, you can add a plugin from repo


```
#!unix
$ cordova plugin add https://bitbucket.org/mngcorp/mngads-cordova-plugin.git#/mng-ads-sdk-cordova-plugin 
```

### Use our www demo

You find an example of implementation on [www] directory

### Implementation

#### Initializing the SDK

You must set the appId provided by mngAds team.

```
#!javascript
    //set appId
    mngadsAppId: function() {
      return (device.platform == "iOS")?'YOUR_IOS_APPID':'YOUR_ANDROID_APPID';
    },
   //Create MngAds Object
    initMngAds: function() {
        // Set your AppId.
        this.mngAds = new MngAdsSDK();
        if (! this.mngAds ) { alert( 'mngAds plugin not ready' ); return; }
        // Connection to the API.
        this.mngAds.initWithAppId(this.mngadsAppId());
        this.createInterstitial();
    },
```


#### Banner

##### Make a request

You must set the placementId provided by mngAds team.

```
#!javascript

/**
 * Create banner.
 *  @param: placementId
 *  @param: height: requested height (dp for android and pt for iOS)
 *  @param: position: TOP or BOTTOM
 *  @param: autoDisplay: if autoDisplay == false, use MngAdsSDK.prototype.showBanner to show it
 *  @param: preferences: (gender, location, keyword...)
 *  @param: successCallback: this callback is called when banner did load
 *  @param: failureCallback: this callback is called when Factory failed to create banner (isBusy,worong placmentID,No ad,Timeout ...)
 */
createBanner: function(id,size,position) {
        var preferences = "{\
                           \"age\": \"25\",\
                           \"language\": \"fr\",\
                           \"keyword\": \"brand=myBrand;category=sport\",\
                           \"gender\": \"M\",\
                           \"location\": {\
                               \"lat\": \"48.876\",\
                               \"lon\": \"10.453\"\
                           }\
                       }";
      document.getElementById("status-ads").innerHTML="waiting "+device.platform;
      // Set your placementId.
      var placementId = '/'+this.mngadsAppId()+'/homebanner';
      this.mngAds.createBanner(placementId,size,position,true,preferences,this.adSuccess,this.adError)
    },
```





#### Interstitial

##### Make a request

You must set the placementId provided by mngAds team.

```
#!javascript

/**
 * Create interstitial.
 *  @param: placementId
 *  @param: preferences: (gender, location, keyword...)
 *  @param: successCallback: this callback is called when interstitial did load or did disappear
 *  @param: failureCallback: this callback is called when Factory failed to create interstitial (isBusy,worong placmentID,No ad,Timeout ...)
 */
createInterstitial: function() {
      var preferences = "{\
                           \"age\": \"25\",\
                           \"language\": \"fr\",\
                           \"keyword\": \"brand=myBrand;category=sport\",\
                           \"gender\": \"M\",\
                           \"location\": {\
                               \"lat\": \"48.876\",\
                               \"lon\": \"10.453\"\
                           }\
                       }";
      document.getElementById("status-ads").innerHTML="waiting "+device.platform;
    // Set your placementId.
    var placementId = '/'+this.mngadsAppId()+'/interstitial';
    this.mngAds.createInterstitial(placementId,preferences,this.adSuccess,this.adError);
      
    },
```


### Build the App


```
#!unix

$ cordova build ios
$ cordova build android
```

[MngAds Ios]:https://bitbucket.org/mngcorp/mngads-demo-ios
[MngAds Android]:https://bitbucket.org/mngcorp/mngads-demo-android
[Cordova CLI]:https://cordova.apache.org/docs/en/edge/guide_cli_index.md.html#The%20Command-Line%20Interface
[mng-ads-sdk-cordova-plugin]:https://bitbucket.org/mngcorp/mngads-cordova-plugin/src/HEAD/mng-ads-sdk-cordova-plugin/?at=master
[www]:https://bitbucket.org/mngcorp/mngads-private-cordova-plugin/src/HEAD/www/?at=master